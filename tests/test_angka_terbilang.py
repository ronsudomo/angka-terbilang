import pytest
from angka_terbilang import angka_terbilang


def test_negative_numbers():
    assert angka_terbilang(-1) == "minus satu"
    assert angka_terbilang(-1234567890) == (
        "minus satu milyar "
        "dua ratus tiga puluh empat juta "
        "lima ratus enam puluh tujuh ribu "
        "delapan ratus sembilan puluh"
    )


def test_numbers_larger_than_supported():
    assert angka_terbilang(1111111111111111111) == (
        "seribu kuadriliun atau lebih"
    )
    assert angka_terbilang(9999999999999999999) == (
        "seribu kuadriliun atau lebih"
    )


def test_numbers_less_than_100():
    assert angka_terbilang(0) == "nol"
    assert angka_terbilang(1) == "satu"
    assert angka_terbilang(2) == "dua"
    assert angka_terbilang(3) == "tiga"
    assert angka_terbilang(4) == "empat"
    assert angka_terbilang(5) == "lima"
    assert angka_terbilang(6) == "enam"
    assert angka_terbilang(7) == "tujuh"
    assert angka_terbilang(8) == "delapan"
    assert angka_terbilang(9) == "sembilan"
    assert angka_terbilang(10) == "sepuluh"
    assert angka_terbilang(11) == "sebelas"
    assert angka_terbilang(12) == "dua belas"
    assert angka_terbilang(15) == "lima belas"
    assert angka_terbilang(19) == "sembilan belas"

    assert angka_terbilang(20) == "dua puluh"
    assert angka_terbilang(25) == "dua puluh lima"
    assert angka_terbilang(50) == "lima puluh"
    assert angka_terbilang(55) == "lima puluh lima"
    assert angka_terbilang(90) == "sembilan puluh"
    assert angka_terbilang(95) == "sembilan puluh lima"


def test_numbers_with_trailing_zeroes():
    assert angka_terbilang(100) == "seratus"
    assert angka_terbilang(1000) == "seribu"
    assert angka_terbilang(10000) == "sepuluh ribu"
    assert angka_terbilang(100000) == "seratus ribu"
    assert angka_terbilang(1000000) == "satu juta"
    assert angka_terbilang(10000000) == "sepuluh juta"
    assert angka_terbilang(100000000) == "seratus juta"
    assert angka_terbilang(1000000000) == "satu milyar"
    assert angka_terbilang(10000000000) == "sepuluh milyar"
    assert angka_terbilang(100000000000) == "seratus milyar"
    assert angka_terbilang(1000000000000) == "satu triliun"
    assert angka_terbilang(10000000000000) == "sepuluh triliun"
    assert angka_terbilang(100000000000000) == "seratus triliun"
    assert angka_terbilang(1000000000000000) == "satu kuadriliun"


def test_numbers_with_trailing_ones():
    assert angka_terbilang(111) == "seratus sebelas"
    assert angka_terbilang(1111) == "seribu seratus sebelas"
    assert angka_terbilang(11111) == "sebelas ribu seratus sebelas"
    assert angka_terbilang(111111) == "seratus sebelas ribu seratus sebelas"
    assert angka_terbilang(1111111) == (
        "satu juta seratus sebelas ribu seratus sebelas"
    )
    assert angka_terbilang(11111111) == (
        "sebelas juta seratus sebelas ribu seratus sebelas"
    )
    assert angka_terbilang(111111111) == (
        "seratus sebelas juta seratus sebelas ribu seratus sebelas"
    )
    assert angka_terbilang(1111111111) == (
        "satu milyar seratus sebelas juta seratus sebelas ribu seratus sebelas"
    )
    assert angka_terbilang(11111111111) == (
        "sebelas milyar seratus sebelas juta seratus sebelas ribu seratus sebelas"
    )
    assert angka_terbilang(111111111111) == (
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(1111111111111) == (
        "satu triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(11111111111111) == (
        "sebelas triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(111111111111111) == (
        "seratus sebelas triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(1111111111111111) == (
        "satu kuadriliun "
        "seratus sebelas triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(11111111111111111) == (
        "sebelas kuadriliun "
        "seratus sebelas triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )
    assert angka_terbilang(111111111111111111) == (
        "seratus sebelas kuadriliun "
        "seratus sebelas triliun "
        "seratus sebelas milyar "
        "seratus sebelas juta "
        "seratus sebelas ribu "
        "seratus sebelas"
    )


def test_numbers_with_trailing_nines():
    assert angka_terbilang(9) == "sembilan"
    assert angka_terbilang(99) == "sembilan puluh sembilan"
    assert angka_terbilang(999) == "sembilan ratus sembilan puluh sembilan"
    assert angka_terbilang(9999) == (
        "sembilan ribu sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(99999) == (
        "sembilan puluh sembilan ribu sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(999999) == (
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(9999999) == (
        "sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(99999999) == (
        "sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(999999999) == (
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(9999999999) == (
        "sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(99999999999) == (
        "sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(999999999999) == (
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(9999999999999) == (
        "sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(99999999999999) == (
        "sembilan puluh sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(999999999999999) == (
        "sembilan ratus sembilan puluh sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(9999999999999999) == (
        "sembilan kuadriliun "
        "sembilan ratus sembilan puluh sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(99999999999999999) == (
        "sembilan puluh sembilan kuadriliun "
        "sembilan ratus sembilan puluh sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )
    assert angka_terbilang(999999999999999999) == (
        "sembilan ratus sembilan puluh sembilan kuadriliun "
        "sembilan ratus sembilan puluh sembilan triliun "
        "sembilan ratus sembilan puluh sembilan milyar "
        "sembilan ratus sembilan puluh sembilan juta "
        "sembilan ratus sembilan puluh sembilan ribu "
        "sembilan ratus sembilan puluh sembilan"
    )


def test_numbers_ending_zero_plus_one():
    assert angka_terbilang(100 + 1) == "seratus satu"
    assert angka_terbilang(1000 + 1) == "seribu satu"
    assert angka_terbilang(10000 + 1) == "sepuluh ribu satu"
    assert angka_terbilang(100000 + 1) == "seratus ribu satu"
    assert angka_terbilang(1000000 + 1) == "satu juta satu"
    assert angka_terbilang(10000000 + 1) == "sepuluh juta satu"
    assert angka_terbilang(100000000 + 1) == "seratus juta satu"
    assert angka_terbilang(1000000000 + 1) == "satu milyar satu"
    assert angka_terbilang(10000000000 + 1) == "sepuluh milyar satu"
    assert angka_terbilang(100000000000 + 1) == "seratus milyar satu"
    assert angka_terbilang(1000000000000 + 1) == "satu triliun satu"
    assert angka_terbilang(10000000000000 + 1) == "sepuluh triliun satu"
    assert angka_terbilang(100000000000000 + 1) == "seratus triliun satu"
    assert angka_terbilang(1000000000000000 + 1) == "satu kuadriliun satu"
    assert angka_terbilang(10000000000000000 + 1) == "sepuluh kuadriliun satu"
    assert angka_terbilang(100000000000000000 + 1) == "seratus kuadriliun satu"
    assert (
        angka_terbilang(1000000000000000000 + 1)
        == "seribu kuadriliun atau lebih"
    )
