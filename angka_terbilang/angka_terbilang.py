from typing import List

# Angka dasar
base_num = (
    "",
    "satu",
    "dua",
    "tiga",
    "empat",
    "lima",
    "enam",
    "tujuh",
    "delapan",
    "sembilan",
    "sepuluh",
    "sebelas",
)

# Group ribuan
thousand_groupings = (
    "",
    "ribu",
    "juta",
    "milyar",
    "triliun",
    "kuadriliun",
)


def angka_terbilang(num: int) -> str:
    """Memberikan jumlah terbilang (dalam Bahasa Indonesia) dari angka num yang diberikan.

    Args:
        num: Angka yg diberikan.

    Retuns:
        Jumlah terbilang dari angka yang diberikan.
    """
    if num < 0:
        return "minus " + angka_terbilang(abs(num))

    if num == 0:
        return "nol"

    num_words = []
    chunks = thousand_split(num)
    chunk_count = len(chunks)

    if chunk_count > len(thousand_groupings):
        return f"seribu {thousand_groupings[-1]} atau lebih"

    for i, n in enumerate(chunks):
        if n > 0:
            grouping = thousand_groupings[chunk_count - i - 1]
            if n == 1 and grouping == "ribu":
                num_words.append("seribu")
            else:
                num_words.append(terbilang_ratusan(n) + " " + grouping)

    return " ".join(num_words).rstrip()


def terbilang_ratusan(num: int) -> str:
    """Memberikan jumlah terbilang untuk angka kurang dari 1000.

    Args:
        num (int): Angka yang diberikan (0 <= num < 1000).
                   AssertionError jika num < 0 atau num >= 1000.

    Returns:
        str: Jumlah terbilang dari angka yang diberikan.
    """
    assert num >= 0 and num < 1000

    if num < 12:
        num_words = base_num[num]
    elif num < 20:
        num_words = base_num[num - 10] + " belas"
    elif num < 100:
        num_words = base_num[num // 10] + " puluh " + base_num[num % 10]
    else:
        angka_ratusan = num // 100
        num_words = (
            "seratus "
            if angka_ratusan == 1
            else base_num[angka_ratusan] + " ratus "
        ) + terbilang_ratusan(num % 100)

    return num_words.rstrip()


def thousand_split(num: int) -> List[int]:
    """Split angka menjadi group ribuan.

    Args:
        num (int): Angka yang akan di split.

    Returns:
        List[int]: Angka yang sudah di split ke dalam group ribuan.
    """
    chunks = []

    while num > 0:
        chunks = [num % 1000] + chunks
        num = num // 1000

    return chunks
